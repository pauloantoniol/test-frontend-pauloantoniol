import styled, { css } from 'styled-components';
import { theme } from '../../styles/theme';
import { below } from '../../styles/media';

export const Container = styled.div``;

export const Wrapper = styled.div`
  width: 100%
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(auto-fill, minmax(170px, 1fr));
  transition: all 0.2s ease-out;

  ${below(
    1197,
    css`
      grid-template-columns: ${({ selected }) =>
        !selected
          ? 'repeat(auto-fill, minmax(170px, 1fr))'
          : 'repeat(auto-fill, minmax(140px, 1fr))'};
      grid-gap: ${({ selected }) => (!selected ? '4rem' : '3rem')};
    `
  )}

  ${below(
    1174,
    css`
      grid-template-columns: repeat(auto-fill, minmax(140px, 1fr));
      grid-gap: ${({ selected }) => (!selected ? '4rem' : '2rem')};
    `
  )}

  ${below(
    998,
    css`
      grid-gap: 2rem;
    `
  )}

  ${below(
    914,
    css`
      grid-template-columns: ${({ selected }) =>
        !selected
          ? 'repeat(auto-fill, minmax(140px, 1fr))'
          : 'repeat(auto-fill, minmax(138px, 1fr))'};
    `
  )}
`;

export const Information = styled.div`
  height: 10em;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Book = styled.a`
  --box-shadow-color: ${({ active }) =>
    active ? theme.primary : 'rgba(0, 0, 0, 1)'};
  --translateY: ${({ active }) => (active ? '-4px' : '0')};

  display: flex;
  flex-direction: column;
  cursor: pointer;
  transform: translateY(var(--translateY));

  &:hover,
  &:focus,
  &:active {
    text-decoration: none;
    color: inherit;
  }

  &:hover {
    --translateY: -4px;
    .container {
      --box-shadow-color: ${theme.primary};
    }
  }

  .container {
    width: 100%;
    overflow: hidden;
    border-radius: 5px;
    margin-bottom: 1em;
    box-shadow: 3px 4px 5px var(--box-shadow-color);

    img {
      width: 100%;
      height: auto;
    }
  }

  .names {
    display: flex;
    flex-direction: column;

    h4 {
      font-size: 1.03em;
      text-decoration: none;
      line-height: 22px;
    }

    & > span {
      font-size: 0.9em;
      color: #555555;
    }
  }
`;
