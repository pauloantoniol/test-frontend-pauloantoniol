import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import Truncate from 'react-truncate';
import empty from 'is-empty';
import { Loader } from 'rsuite';

import showDrawer from '../_functions/showDrawer';

import { Container, Wrapper, Book, Information } from './styles';

export default function ListBooks({ books, selected, loading }) {
  let information;
  if (loading) {
    information = (
      <Loader
        size="lg"
        content={
          process.env.NODE_ENV === 'production' && (
            <>
              <h3>Waiting for Heroku! </h3>
              <h4>if it persists then reload the page.</h4>
            </>
          )
        }
        vertical
      />
    );
  } else if (empty(books)) {
    information = <h3>No books!</h3>;
  }

  const bookRef = useRef(null);
  useEffect(() => {
    if (!empty(bookRef.current)) {
      setTimeout(() => {
        bookRef.current.scrollIntoView({ behavior: 'smooth' });
      }, 300);
    }
  }, [selected]);

  return (
    <Container>
      {!empty(information) ? (
        <Information>{information}</Information>
      ) : (
        <Wrapper selected={selected}>
          {books.map(book => (
            <Book
              key={book.id}
              ref={selected === book.id ? bookRef : null}
              title={`${book.title} | ${book.author}`}
              onClick={() => {
                const toggle = selected === book.id ? null : book.id;
                showDrawer(toggle);
              }}
              active={selected === book.id}
            >
              <div className="container">
                <img src={book.cover} alt={book.title} />
              </div>
              <div className="names">
                <h4>
                  <Truncate lines={2} width={190}>
                    {book.title}
                  </Truncate>
                </h4>
                <span>by {book.author}</span>
              </div>
            </Book>
          ))}
        </Wrapper>
      )}
    </Container>
  );
}

ListBooks.propTypes = {
  books: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      cover: PropTypes.string,
      author: PropTypes.string,
    })
  ),
  selected: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  loading: PropTypes.bool,
};

ListBooks.defaultProps = {
  books: [],
  selected: null,
  loading: false,
};
