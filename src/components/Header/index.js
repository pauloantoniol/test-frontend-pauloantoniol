import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IconStack, Icon, Avatar } from 'rsuite';
import empty from 'is-empty';

import { Container, Wrapper } from './styles';
import { theme } from '../../styles/theme';
import getSelected from '../_functions/getStore';
import showDrawer from '../_functions/showDrawer';

import { theBooksFilterRequest } from '../../store/modules/theBooks/actions';

import Filter from '../Filter';

export default function Header() {
  const dispatch = useDispatch();
  const { selected } = useSelector(state => state.theDrawerOptions);

  const handleFilter = useCallback(
    title => {
      if (!empty(getSelected())) {
        showDrawer(null);
      }
      dispatch(theBooksFilterRequest({ title }));
    },
    [dispatch]
  );

  useEffect(() => {
    handleFilter('');
  }, [handleFilter]);

  return (
    <Container className={selected}>
      <Wrapper>
        <IconStack size="2x" style={{ cursor: 'pointer' }}>
          <Icon
            icon="square"
            stack="2x"
            style={{
              color: theme.primary,
            }}
          />
          <Icon icon="book" stack="1x" />
        </IconStack>
        <Filter onSearch={handleFilter} />
        <Avatar
          circle
          size="lg"
          src="https://avatars3.githubusercontent.com/u/8494360?s=460&v=4"
          alt="PA"
          style={{ cursor: 'pointer' }}
        />
      </Wrapper>
    </Container>
  );
}
