import styled from 'styled-components';
import Center from '../../styles/Center';

export const Container = styled.header`
  position: relative;
  width: 100%;
  height: 10rem;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.4);
  z-index: 2;
  margin-bottom: 2rem;
`;

export const Wrapper = styled(Center)`
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
