import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper } from './styles';
import Header from '../Header';
import TheDrawer from '../TheDrawer';

export default function MainLayout({ children: page }) {
  return (
    <Wrapper>
      <Header />
      {page}
      <TheDrawer />
    </Wrapper>
  );
}

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
