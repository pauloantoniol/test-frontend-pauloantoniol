import PropTypes from 'prop-types';
import store from '../../store';

import { theDrawerOptionsRequest } from '../../store/modules/theDrawerOptions/actions';

export default function showDrawer(id, edit) {
  store.dispatch(theDrawerOptionsRequest({ id, edit }));
}

showDrawer.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  edit: PropTypes.bool,
};

showDrawer.defaultProps = {
  id: null,
  edit: false,
};
