import store from '../../store';

export default function getSelected() {
  return store.getState().theDrawerOptions.selected;
}
