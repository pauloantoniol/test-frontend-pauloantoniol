import styled, { css } from 'styled-components';
import { below } from '../../styles/media';
import { theme } from '../../styles/theme';

export const Container = styled.div`
  /*INPUT SEARCH RSUITE A*/
  width: 60rem;

  input.rs-input.rs-input-lg {
    box-sizing: border-box;
  }

  input.rs-input:hover,
  input.rs-input:focus {
    border-color: ${theme.primary};
  }

  a.rs-btn.rs-input-group-btn {
    right: 2px;
    border-bottom-right-radius: 7px;
    border-top-right-radius: 7px;
    height: 40px;

    i.rs-icon-search {
      font-size: 16px;
      line-height: 1.6;
    }
  }

  ${below(
    'lg',
    css`
      width: 50%;
    `
  )}
`;
