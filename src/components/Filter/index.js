import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { InputGroup, Input, Icon } from 'rsuite';

import { Container } from './styles';

let timer = null;

export default function Filter({ onSearch }) {
  const [sFilter, setSFilter] = useState('');

  function handleFiltration(event) {
    clearTimeout(timer);
    const currentValue = typeof event === 'string' ? event : event.target.value;
    if (event.type === 'keydown' && event.keyCode === 13) {
      onSearch(currentValue);
    } else if (typeof event !== 'object') {
      setSFilter(currentValue);
      timer = setTimeout(() => {
        onSearch(currentValue);
      }, 300);
    }
  }

  return (
    <Container>
      <InputGroup inside>
        <Input
          size="lg"
          type="search"
          placeholder="Find a book"
          value={sFilter}
          onChange={handleFiltration}
          onKeyDown={handleFiltration}
        />
        <InputGroup.Button
          onClick={() => {
            handleFiltration(sFilter);
          }}
        >
          <Icon icon="search" />
        </InputGroup.Button>
      </InputGroup>
    </Container>
  );
}

Filter.propTypes = {
  onSearch: PropTypes.func,
};

Filter.defaultProps = {
  onSearch: null,
};
