import React, { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import empty from 'is-empty';
import swal from 'sweetalert';

import {
  Drawer,
  Divider,
  Button,
  Uploader,
  Form,
  FormControl,
  Schema,
  Loader,
  Alert,
} from 'rsuite';
import {
  DrawerBody,
  DrawerImage,
  DrawerTitle,
  DrawerAuthor,
  DrawerDescription,
  DrawerInformations,
  DrawerFooter,
} from './styles';
import { theme } from '../../styles/theme';

import showDrawer from '../_functions/showDrawer';
import {
  theBooksUpdateRequest,
  theBooksDeleteRequest,
} from '../../store/modules/theBooks/actions';
import cover from '../../assets/no-cover.png';

const { StringType, NumberType } = Schema.Types;
const model = Schema.Model({
  title: StringType().isRequired('This field is required.'),
  author: StringType().isRequired('This field is required.'),
  description: StringType().isRequired('This field is required.'),
  pages: NumberType()
    .min(1, 'The field cannot be less than 1 page')
    .isRequired('This field is required.'),
  dimensions: StringType().isRequired('This field is required.'),
});
const defaultBookInformation = {
  title: '',
  author: '',
  description: '',
  pages: 0,
  dimensions: '',
  cover,
};
let wasWorkingOnNew = false;

export default function TheDrawer() {
  const dispatch = useDispatch();
  const [uploadingImage, setUploadingImage] = useState(false);
  const [bookInformation, setBookInformation] = useState(
    defaultBookInformation
  );
  const [bookErros, setBookErros] = useState({});

  let buttonLayout;
  const TheForm = useRef(null);
  const titleEditRef = useRef(null);
  const bodyRef = useRef(null);

  const { books, saveLoading } = useSelector(state => state.theBooks);
  const { selected, edit } = useSelector(state => state.theDrawerOptions);
  const book = books.find(bookE => bookE.id === selected) || null;

  useEffect(() => {
    if (!empty(bodyRef.current)) {
      bodyRef.current.offsetParent.scrollTo(0, 0);
    }
  }, [book]);

  useEffect(() => {
    if (edit) {
      setBookInformation(book || defaultBookInformation);

      setTimeout(() => {
        const target = titleEditRef.current.children[0].firstElementChild;
        target.style.height = 'auto';
        target.style.height = `${target.scrollHeight + 2}px`;
      }, 50);

      if (!empty(TheForm.current)) {
        TheForm.current.cleanErrors();
      }
    }
  }, [edit, book]);

  useEffect(() => {
    if (wasWorkingOnNew && !saveLoading) {
      wasWorkingOnNew = false;
      showDrawer(books[books.length - 1].id);
    }
  }, [saveLoading, books]);

  if (edit) {
    if (selected === 'new') {
      buttonLayout = (
        <Button
          onClick={() => {
            if (TheForm.current.check()) {
              dispatch(
                theBooksUpdateRequest({
                  book: bookInformation,
                  newBook: true,
                })
              );
              wasWorkingOnNew = true;
            } else {
              Alert.error('There are fields required');
            }
          }}
          appearance="primary"
          style={{
            backgroundColor: theme.primary,
          }}
        >
          Save
        </Button>
      );
    } else {
      buttonLayout = (
        <>
          <Button
            onClick={() => {
              showDrawer(selected);
            }}
            style={{
              backgroundColor: '#1a1d24',
            }}
          >
            Cancel
          </Button>
          <Button
            onClick={async () => {
              if (TheForm.current.check()) {
                await dispatch(
                  theBooksUpdateRequest({ book: bookInformation })
                );
                await showDrawer(selected);
              } else {
                Alert.error('There are fields required');
              }
            }}
            appearance="primary"
            style={{
              backgroundColor: theme.primary,
            }}
          >
            Save
          </Button>
        </>
      );
    }
  } else {
    buttonLayout = (
      <>
        <Button
          onClick={() => {
            swal({
              title: 'Want to delete this book?',
              icon: 'warning',
              buttons: {
                cancel: true,
                confirm: { text: 'Yes', closeModal: false },
              },
              dangerMode: true,
            }).then(async confirm => {
              if (confirm) {
                await dispatch(theBooksDeleteRequest({ id: book.id }));
                showDrawer(null);
                swal.close();
              }
            });
          }}
          style={{
            backgroundColor: '#1a1d24',
          }}
        >
          Remove
        </Button>
        <Button
          onClick={() => {
            showDrawer(selected, true);
          }}
          appearance="primary"
          style={{
            backgroundColor: theme.primary,
          }}
        >
          Edit
        </Button>
      </>
    );
  }

  return (
    <>
      {!empty(selected) && (
        <Drawer
          size="xs"
          backdrop={false}
          show
          onHide={() => {
            showDrawer(null);
          }}
        >
          {(uploadingImage || saveLoading) && (
            <Loader backdrop center style={{ zIndex: 1 }} />
          )}
          <Form
            ref={TheForm}
            className="Formulario"
            onChange={value => {
              setBookInformation({ ...bookInformation, ...value });
            }}
            onCheck={formError => {
              setBookErros({ ...bookErros, ...formError });
            }}
            formValue={bookInformation}
            model={model}
          >
            <Drawer.Header></Drawer.Header>
            <Drawer.Body
              style={{
                paddingBottom: '5em',
                paddingRight: '1em',
                marginRight: '1em',
              }}
            >
              <DrawerBody ref={bodyRef}>
                <DrawerImage>
                  {edit ? (
                    <Uploader
                      fileListVisible={false}
                      action="//jsonplaceholder.typicode.com/photos/"
                      listType="picture"
                      onUpload={file => {
                        setUploadingImage(true);
                        // TRANSFORM IN BLOB
                        const reader = new FileReader();
                        reader.onloadend = () => {
                          setBookInformation({
                            ...bookInformation,
                            cover: reader.result,
                          });
                        };
                        reader.readAsDataURL(file.blobFile);
                      }}
                      onSuccess={() => {
                        setUploadingImage(false);
                      }}
                      onError={() => {
                        setUploadingImage(false);
                      }}
                    >
                      <button type="button" className="buttonImage">
                        <img
                          src={bookInformation.cover}
                          alt={bookInformation.title}
                        />
                      </button>
                    </Uploader>
                  ) : (
                    <img src={book.cover} alt={book.title} />
                  )}
                </DrawerImage>
                <DrawerTitle ref={titleEditRef}>
                  {edit ? (
                    <FormControl
                      name="title"
                      componentClass="textarea"
                      placeholder="Book Title"
                      onChange={(_, { target }) => {
                        target.style.height = 'auto';
                        target.style.height = `${target.scrollHeight + 2}px`;
                      }}
                      maxLength={100}
                      autoComplete="off"
                    />
                  ) : (
                    book.title
                  )}
                </DrawerTitle>
                <DrawerAuthor>
                  {edit ? (
                    <FormControl
                      name="author"
                      placeholder="Book Author"
                      style={{ width: '100%' }}
                      maxLength={30}
                      autoComplete="off"
                    />
                  ) : (
                    book.author
                  )}
                </DrawerAuthor>
                <DrawerDescription>
                  {edit ? (
                    <FormControl
                      name="description"
                      rows={30}
                      style={{ width: '100%' }}
                      componentClass="textarea"
                      placeholder="Book description"
                      maxLength={800}
                      autoComplete="off"
                    />
                  ) : (
                    <p>{book.description}</p>
                  )}
                </DrawerDescription>
                <Divider />
                <DrawerInformations>
                  <span>
                    <b>Pages:</b>
                    {edit ? (
                      <FormControl
                        name="pages"
                        style={{ width: 90 }}
                        type="number"
                        min={0}
                        autoComplete="off"
                      />
                    ) : (
                      book.pages
                    )}
                  </span>
                  <span>
                    <b>Dimensions:</b>
                    {edit ? (
                      <FormControl
                        name="dimensions"
                        style={{ width: 155 }}
                        maxLength={50}
                        autoComplete="off"
                      />
                    ) : (
                      book.dimensions
                    )}
                  </span>
                </DrawerInformations>
              </DrawerBody>
            </Drawer.Body>
            <DrawerFooter>{buttonLayout}</DrawerFooter>
          </Form>
        </Drawer>
      )}
    </>
  );
}
