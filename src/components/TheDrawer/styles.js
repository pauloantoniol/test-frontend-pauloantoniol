import styled from 'styled-components';

export const DrawerBody = styled.div`
  width: 95%;
  margin: auto;
`;

export const DrawerImage = styled.div`
  width: 170px;
  margin: 0 auto 1.5em;
  overflow: hidden;
  border-radius: 5px;
  box-shadow: 3px 4px 5px rgba(0, 0, 0, 1);

  img,
  button.rs-uploader-trigger-btn {
    width: 100%;
    height: auto;
    margin: 0;
    border: none;
  }

  button:hover {
    opacity: 0.6;
  }

  .rs-uploader-picture .rs-uploader-trigger {
    float: inherit;
  }
`;

export const DrawerTitle = styled.h4`
  width: 100%;
  font-size: 1.4em;

  div {
    display: unset;
  }
  textarea {
    font-size: 1em;
    min-width: 100%;
    min-height: 72px;
    max-height: none;
    margin-bottom: 0.3em;
  }
`;

export const DrawerAuthor = styled.span`
  width: 100%;
  font-size: 1em;
  color: #666666;
`;

export const DrawerDescription = styled.div`
  margin-top: 1em;

  textarea {
    font-size: 1em;
    min-width: 100%;
    max-height: 250px;
    width: 100% !important;
  }
`;

export const DrawerInformations = styled.div`
  display: flex;
  flex-direction: column;
  span {
    margin-bottom: 10px;
    display: flex;
    align-items: center;
    b {
      font-weight: 600;
      margin-right: 0.5em;
    }
  }
`;

export const DrawerFooter = styled.div`
  box-shadow: 0 -3px 8px rgba(0, 0, 0, 0.2);
  width: 100%;
  background: #292d33;
  position: absolute;
  bottom: 0;
  padding: 1em 20px;
  display: flex;
  justify-content: flex-end;

  button {
    font-weight: 600;
    margin-left: 1em;
  }
`;
