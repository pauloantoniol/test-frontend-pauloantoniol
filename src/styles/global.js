import { createGlobalStyle, css } from 'styled-components';
import { below } from './media';
import { theme } from './theme';

const isWindows = navigator.platform.toUpperCase().indexOf('WIN') >= 0;

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap');

  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;
    ${below(
      'lg',
      css`
        font-size: 58%;
      `
    )}
  }


  body, input, textarea, button {
    font-family: 'Montserrat', sans-serif;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    font-size: 1.6rem;
  }

  input[type=number]{
    text-align: right;
  }

  button{
    cursor: pointer;
  }

  a {
    text-decoration: none;
    color: inherit;
  }

  .rs-alert-item-wrapper{ margin-left: 0; }
  .rs-alert-container{left: 2%}
  .rs-uploader-trigger input[type='file']{ width: 1em; }

  ${below(
    414,
    css`
      .rs-drawer-left.rs-drawer-xs,
      .rs-drawer-right.rs-drawer-xs {
        width: 100%;
      }
    `
  )}

  .swal-button--danger:not([disabled]):hover,
  .swal-button--danger:active,
  .swal-button--danger{
    background-color: ${theme.primary}
  }

  input[type="search"]::-webkit-search-decoration,
  input[type="search"]::-webkit-search-cancel-button,
  input[type="search"]::-webkit-search-results-button,
  input[type="search"]::-webkit-search-results-decoration{
    -webkit-appearance:none;
  }

  ${() => {
    if (isWindows) {
      return `
        *::-webkit-scrollbar { background-color:#0f131a; width:16px }
        *::-webkit-scrollbar-track { background-color: transparent }
        *::-webkit-scrollbar-thumb { background-color:#babac0; border-radius:16px; border:4px solid #0f131a}
        *::-webkit-scrollbar-button {display:none}
      `;
    }
    return '';
  }}
}
`;
