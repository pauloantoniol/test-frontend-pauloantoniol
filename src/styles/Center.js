import styled, { css } from 'styled-components';
import { below, above } from './media';
import getSelected from '../components/_functions/getStore';

export default styled.div`
  ${above(
    'lg',
    css`
      width: ${() => (!getSelected() ? '85%' : '65%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 32% 0 auto')};
    `
  )}

  ${below(
    'lg',
    css`
      width: ${() => (!getSelected() ? '85%' : '55%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 40% 0 auto')};
    `
  )}

  ${below(
    1062,
    css`
      margin: ${() => (!getSelected() ? '0 auto' : '0 42% 0 auto')};
    `
  )}

  ${below(
    995,
    css`
      width: ${() => (!getSelected() ? '85%' : '50%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 46% 0 auto')};
    `
  )}

  ${below(
    899,
    css`
      width: ${() => (!getSelected() ? '85%' : '45%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 51% 0 auto')};
    `
  )}

  ${below(
    796,
    css`
      width: ${() => (!getSelected() ? '85%' : '42%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 54% 0 auto')};
    `
  )}

  ${below(
    738,
    css`
      width: ${() => (!getSelected() ? '85%' : '40%')};
      margin: ${() => (!getSelected() ? '0 auto' : '0 57% 0 auto')};
    `
  )}

  ${below(
    600,
    css`
      width: 85%;
      margin: 0 auto;
    `
  )}


`;
