import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import Route from './Route';

import Entrance from '../pages/Entrance';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Entrance} />
      <Redirect to="/" />
    </Switch>
  );
}
