import React from 'react';
import { useSelector } from 'react-redux';
import empty from 'is-empty';
import { Icon, Button, Divider } from 'rsuite';

import { Container, WrapperTitle, Title } from './styles';
import { theme } from '../../styles/theme';
import showDrawer from '../../components/_functions/showDrawer';

import ListBooks from '../../components/ListBooks';

export default function Entrance() {
  const { books, qtd, filterLoading } = useSelector(state => state.theBooks);
  const { selected } = useSelector(state => state.theDrawerOptions);

  return (
    <Container>
      <WrapperTitle selected={selected}>
        <Title selected={selected}>
          <h1>Manga List</h1>
          <div>
            <Icon icon="bookmark" size="2x" />
            <span>{!empty(qtd) && !filterLoading ? qtd : 0}</span>
          </div>
        </Title>
        <Button
          onClick={() => {
            const toggle = selected === 'new' ? null : 'new';
            const edit = selected === 'new';
            showDrawer(toggle, edit);
          }}
          appearance="primary"
          style={{
            fontWeight: '600',
            backgroundColor: theme.primary,
          }}
        >
          New Book
        </Button>
      </WrapperTitle>
      <Divider />
      <ListBooks books={books} selected={selected} loading={filterLoading} />
    </Container>
  );
}
