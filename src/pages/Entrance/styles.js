import styled, { css } from 'styled-components';
import Center from '../../styles/Center';
import { below } from '../../styles/media';
import { theme } from '../../styles/theme';

export const Container = styled(Center)`
  display: block;
`;

export const WrapperTitle = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${below(
    995,
    css`
      flex-direction: ${({ selected }) => (!selected ? 'row' : 'column')};
      align-items: ${({ selected }) => (!selected ? 'center' : 'flex-end')};
    `
  )}

  ${below(
    550,
    css`
      flex-direction: column;
      align-items: flex-end;
    `
  )}
`;

export const Title = styled.div`
  width: 20em;
  display: flex;
  justify-content: space-between;
  align-items: center;

  h1 {
    font-size: 4rem;
  }

  div {
    display: flex;
    align-items: center;

    & > * {
      margin-right: 0.5em;
      color: ${theme.primary};
    }

    span {
      font-size: 2rem;
      font-weight: 600;
    }
  }

  ${below(
    819,
    css`
      flex-direction: ${({ selected }) => (!selected ? 'row' : 'column')};
      align-items: ${({ selected }) => (!selected ? 'center' : 'flex-end')};
      margin-bottom: ${({ selected }) => (!selected ? '0' : '1em')};
    `
  )}

  ${below(
    445,
    css`
      flex-direction: column;
      align-items: flex-end
      margin-bottom: 1em
    `
  )}
`;
