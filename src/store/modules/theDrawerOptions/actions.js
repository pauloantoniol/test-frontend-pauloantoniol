export function theDrawerOptionsRequest(collection) {
  return {
    type: '@theDrawerOptions/REQUEST',
    payload: collection,
  };
}

export function theDrawerOptionsSuccess(payload) {
  return {
    type: '@theDrawerOptions/SUCCESS',
    payload,
  };
}

export function theDrawerOptionsFailure() {
  return {
    type: '@theDrawerOptions/FAILURE',
  };
}
