import { takeLatest, put, all } from 'redux-saga/effects';
import empty from 'is-empty';

import { theDrawerOptionsSuccess, theDrawerOptionsFailure } from './actions';

export function* theDrawerStart({ payload }) {
  try {
    const { id, edit } = payload;
    const selected = empty(id) ? null : id;
    yield put(theDrawerOptionsSuccess({ selected, edit }));
  } catch (err) {
    console.tron.error(err);
    yield put(theDrawerOptionsFailure());
  }
}

export default all([takeLatest('@theDrawerOptions/REQUEST', theDrawerStart)]);
