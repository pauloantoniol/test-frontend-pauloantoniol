import produce from 'immer';
import empty from 'is-empty';

const INITIAL_STATE = {
  selected: null,
  edit: false,
  theDrawerOptionsLoading: false,
};

export default function theDrawerOptions(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@theDrawerOptions/REQUEST': {
        draft.theDrawerOptionsLoading = true;
        break;
      }
      case '@theDrawerOptions/SUCCESS': {
        const { selected, edit } = action.payload;
        draft.selected = selected;
        draft.edit = !empty(edit);
        draft.theDrawerOptionsLoading = false;
        break;
      }
      case '@theDrawerOptions/FAILURE': {
        draft.theDrawerOptionsLoading = false;
        break;
      }
      default:
    }
  });
}
