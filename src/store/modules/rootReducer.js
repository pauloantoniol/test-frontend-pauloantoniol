import { combineReducers } from 'redux';

import theBooks from './theBooks/reducer';
import theDrawerOptions from './theDrawerOptions/reducer';

export default combineReducers({ theBooks, theDrawerOptions });
