import { takeLatest, call, put, all } from 'redux-saga/effects';
import empty from 'is-empty';
import { Alert } from 'rsuite';

import api from '../../../services/api';

import {
  theBooksFilterSuccess,
  theBooksUpdateSuccess,
  theBooksDeleteSuccess,
  theBooksFailure,
} from './actions';

export function* filterStart({ payload }) {
  try {
    const { id, title } = payload;
    const filter = !empty(id)
      ? `/${id}`
      : `?${!empty(title) ? `&title_like=${title}` : ''}`;
    const URL = `books${filter}`;

    const responseFilter = yield call(api.get, URL);
    const books = [...responseFilter.data];
    const qtd = books.length;

    yield put(theBooksFilterSuccess({ books, qtd }));
  } catch (err) {
    Alert(err);
    console.tron.error(err);
    yield put(theBooksFailure());
  }
}

export function* saveBookStart({ payload }) {
  try {
    const { book, newBook } = payload;
    const method = newBook ? 'post' : 'put';
    const URL = `books${newBook ? '' : `/${book.id}`}`;

    const responseSave = yield call(api[method], URL, book);
    yield put(theBooksUpdateSuccess({ book: responseSave.data, newBook }));
  } catch (err) {
    Alert(err);
    console.tron.error(err);
    yield put(theBooksFailure());
  }
}

export function* deleteBookStart({ payload }) {
  try {
    const { id } = payload;
    yield call(api.delete, `books/${id}`);
    yield put(theBooksDeleteSuccess({ id }));
  } catch (err) {
    Alert(err);
    console.tron.error(err);
    yield put(theBooksFailure());
  }
}

export default all([
  takeLatest('@theBooks/UPDATE/REQUEST', saveBookStart),
  takeLatest('@theBooks/DELETE/REQUEST', deleteBookStart),
  takeLatest('@theBooks/FILTER/REQUEST', filterStart),
]);
