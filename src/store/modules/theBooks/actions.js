export function theBooksFilterRequest(payload) {
  return {
    type: '@theBooks/FILTER/REQUEST',
    payload,
  };
}
export function theBooksFilterSuccess(payload) {
  return {
    type: '@theBooks/FILTER/SUCCESS',
    payload,
  };
}

export function theBooksUpdateRequest(payload) {
  return {
    type: '@theBooks/UPDATE/REQUEST',
    payload,
  };
}
export function theBooksUpdateSuccess(payload) {
  return {
    type: '@theBooks/UPDATE/SUCCESS',
    payload,
  };
}

export function theBooksDeleteRequest(payload) {
  return {
    type: '@theBooks/DELETE/REQUEST',
    payload,
  };
}
export function theBooksDeleteSuccess(payload) {
  return {
    type: '@theBooks/DELETE/SUCCESS',
    payload,
  };
}

export function theBooksFailure() {
  return {
    type: '@theBooks/FAILURE',
  };
}
