import produce from 'immer';
import { Alert } from 'rsuite';

const INITIAL_STATE = {
  books: [],
  qtd: 0,
  filterLoading: false,
  saveLoading: false,
};

export default function theBooks(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@theBooks/FILTER/REQUEST': {
        draft.filterLoading = true;
        break;
      }
      case '@theBooks/FILTER/SUCCESS': {
        const { books, qtd } = action.payload;
        draft.books = books;
        draft.qtd = qtd;
        draft.filterLoading = false;
        break;
      }

      case '@theBooks/UPDATE/REQUEST': {
        draft.saveLoading = true;
        break;
      }
      case '@theBooks/UPDATE/SUCCESS': {
        const { book, newBook } = action.payload;
        if (newBook) {
          const booksTemp = [...draft.books];
          booksTemp.push(book);
          draft.books = booksTemp;
          draft.qtd = booksTemp.length;
        } else {
          const index = draft.books.findIndex(el => el.id === book.id);
          draft.books[index] = book;
        }
        draft.saveLoading = false;
        Alert.success('Book saved!');
        break;
      }

      case '@theBooks/DELETE/REQUEST': {
        break;
      }
      case '@theBooks/DELETE/SUCCESS': {
        const { id } = action.payload;
        const index = draft.books.findIndex(el => el.id === id);
        const booksTemp = [...draft.books];
        booksTemp.splice(index, 1);
        draft.books = booksTemp;
        draft.qtd = booksTemp.length;
        break;
      }

      case '@theBooks/FAILURE': {
        draft.filterLoading = false;
        draft.saveLoading = false;
        break;
      }
      default:
    }
  });
}
