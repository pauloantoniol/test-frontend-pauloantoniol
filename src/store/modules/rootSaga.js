import { all } from 'redux-saga/effects';

import theBooks from './theBooks/sagas';
import theDrawerOptions from './theDrawerOptions/sagas';

export default function* rootSaga() {
  return yield all([theBooks, theDrawerOptions]);
}
